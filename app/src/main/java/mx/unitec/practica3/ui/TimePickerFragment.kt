package mx.unitec.practica3.ui

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_picker_time.*
import java.util.*


class TimePickerFragment: DialogFragment(), TimePickerDialog.OnTimeSetListener {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c=Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute =  c.get(Calendar.MINUTE)

        return TimePickerDialog(activity, this, hour, minute, DateFormat.is24HourFormat(activity))
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {

        pkrTime.setText(hourOfDay.toString()+ ":"+minute.toString())

        Toast.makeText(activity,"${hourOfDay}:${minute}", Toast.LENGTH_LONG).show()
    }

}