package mx.unitec.practica3.ui

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.DatePicker
import android.widget.TextView
import android.widget.TimePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_picker_time.*
import java.util.*


class DatePickerFragment: DialogFragment(), DatePickerDialog.OnDateSetListener {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c=Calendar.getInstance()
        val ano = c.get(Calendar.YEAR)
        val mes =  c.get(Calendar.MONTH)
        val day =  c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(activity, this, ano, mes,day, DateFormat))
    }

    override fun onDataset(view: DatePicker?, hourOfDay: Int, minute: Int) {

        pkrTime.setText(hourOfDay.toString()+ ":"+minute.toString())

        Toast.makeText(activity,"${hourOfDay}:${minute}", Toast.LENGTH_LONG).show()
    }

}