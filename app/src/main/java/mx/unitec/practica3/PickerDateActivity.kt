package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import mx.unitec.practica3.ui.TimePickerFragment

class PickerDateActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_date)
    }

    fun showDatePickerDialog(s: View){
        val timePickerFragment = TimePickerFragment()
        timePickerFragment.show(supportFragmentManager,"timePicker")

    }
}