package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast

class AutoTextActivity : AppCompatActivity() {

    lateinit var autoTextView:AutoCompleteTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_text)

        autoTextView =  findViewById(R.id.autoCompleteTextViewAlcaldia)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item
        )

        autoTextView.threshold = 2
        
        autoTextView.setAdapter(adapter)
        
        autoTextView.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this,
                position.toString() + " : " +parent?.getItemAtPosition(position).toString() ,
                Toast.LENGTH_SHORT).show()
        }

    }
}